package com.klymchuk;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
    private static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        logger.trace("This is a trace logger");
        logger.debug("This is a debug logger");
        logger.info("This is a info logger");
        logger.warn("This is a string logger");
        logger.error("This is an error logger");
        logger.fatal("This is a fatal logger");
    }


}
